-- MariaDB dump 10.19  Distrib 10.6.4-MariaDB, for osx10.15 (x86_64)
--
-- Host: localhost    Database: kelas_pintar
-- ------------------------------------------------------
-- Server version	10.6.4-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chapters`
--

DROP TABLE IF EXISTS `chapters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapters` (
  `chapter_id` int(11) NOT NULL AUTO_INCREMENT,
  `chapter_name` varchar(150) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`chapter_id`),
  UNIQUE KEY `chapters_UN` (`chapter_name`,`subject_id`),
  KEY `chapters_FK` (`subject_id`),
  CONSTRAINT `chapters_FK` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapters`
--

LOCK TABLES `chapters` WRITE;
/*!40000 ALTER TABLE `chapters` DISABLE KEYS */;
INSERT INTO `chapters` VALUES (1,'bab1',1,NULL,NULL),(6,'bab1',2,'2021-11-16 18:50:51','2021-11-16 18:50:51'),(7,'bab2',1,'2021-11-16 18:53:01','2021-11-16 18:53:01');
/*!40000 ALTER TABLE `chapters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `grade` varchar(2) DEFAULT NULL,
  `score` decimal(10,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`report_id`),
  UNIQUE KEY `report_UN` (`student_id`,`chapter_id`),
  KEY `report_FK_1` (`chapter_id`),
  CONSTRAINT `report_FK` FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`),
  CONSTRAINT `report_FK_1` FOREIGN KEY (`chapter_id`) REFERENCES `chapters` (`chapter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` VALUES (5,1,1,'1',90,'2021-11-16 20:33:05','2021-11-16 20:33:05'),(8,1,6,'1',80,'2021-11-16 20:50:34','2021-11-16 20:50:34'),(10,2,6,'1',90,'2021-11-16 20:51:44','2021-11-16 20:51:44'),(11,2,7,'2',60,'2021-11-16 20:51:49','2021-11-16 20:51:49'),(12,3,1,'1',70,'2021-11-16 20:33:05','2021-11-16 20:33:05'),(15,4,1,'1',50,'2021-11-17 05:45:17','2021-11-17 05:45:17');
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'maman',NULL,NULL),(2,'hadil','2021-11-16 16:45:42','2021-11-16 16:45:42'),(3,'Aziz','2021-11-16 17:36:41','2021-11-16 17:36:41'),(4,'ibnu','2021-11-16 17:37:26','2021-11-16 17:37:26');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(150) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` VALUES (1,'Math','2021-11-16 17:25:07','2021-11-16 17:25:07'),(2,' English','2021-11-16 17:26:11','2021-11-16 17:26:11'),(3,'Indonesian','2021-11-16 17:34:24','2021-11-16 17:34:24'),(4,'Science','2021-11-16 17:34:45','2021-11-16 17:34:45');
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_average_by_grade`
--

DROP TABLE IF EXISTS `v_average_by_grade`;
/*!50001 DROP VIEW IF EXISTS `v_average_by_grade`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_average_by_grade` (
  `report_id` tinyint NOT NULL,
  `student_id` tinyint NOT NULL,
  `grade` tinyint NOT NULL,
  `score` tinyint NOT NULL,
  `chapter_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `chapter_name` tinyint NOT NULL,
  `subject_id` tinyint NOT NULL,
  `subject_name` tinyint NOT NULL,
  `average_score` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_average_by_subject`
--

DROP TABLE IF EXISTS `v_average_by_subject`;
/*!50001 DROP VIEW IF EXISTS `v_average_by_subject`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_average_by_subject` (
  `report_id` tinyint NOT NULL,
  `student_id` tinyint NOT NULL,
  `grade` tinyint NOT NULL,
  `score` tinyint NOT NULL,
  `chapter_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `chapter_name` tinyint NOT NULL,
  `subject_id` tinyint NOT NULL,
  `subject_name` tinyint NOT NULL,
  `average_score` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_max_average`
--

DROP TABLE IF EXISTS `v_max_average`;
/*!50001 DROP VIEW IF EXISTS `v_max_average`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_max_average` (
  `student_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `top` tinyint NOT NULL,
  `subject_name` tinyint NOT NULL,
  `chapter_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_min_average`
--

DROP TABLE IF EXISTS `v_min_average`;
/*!50001 DROP VIEW IF EXISTS `v_min_average`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_min_average` (
  `student_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `top` tinyint NOT NULL,
  `chapter_id` tinyint NOT NULL,
  `subject_name` tinyint NOT NULL,
  `chapter_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'kelas_pintar'
--

--
-- Final view structure for view `v_average_by_grade`
--

/*!50001 DROP TABLE IF EXISTS `v_average_by_grade`*/;
/*!50001 DROP VIEW IF EXISTS `v_average_by_grade`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`taufiq32`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_average_by_grade` AS select `r`.`report_id` AS `report_id`,`r`.`student_id` AS `student_id`,`r`.`grade` AS `grade`,`r`.`score` AS `score`,`r`.`chapter_id` AS `chapter_id`,`s`.`name` AS `name`,`c`.`chapter_name` AS `chapter_name`,`s2`.`subject_id` AS `subject_id`,`s2`.`subject_name` AS `subject_name`,avg(`r`.`score`) AS `average_score` from (((`report` `r` join `students` `s` on(`r`.`student_id` = `s`.`student_id`)) join `chapters` `c` on(`r`.`chapter_id` = `c`.`chapter_id`)) join `subjects` `s2` on(`s2`.`subject_id` = `c`.`subject_id`)) group by `r`.`grade`,`s2`.`subject_id`,`r`.`student_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_average_by_subject`
--

/*!50001 DROP TABLE IF EXISTS `v_average_by_subject`*/;
/*!50001 DROP VIEW IF EXISTS `v_average_by_subject`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`taufiq32`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_average_by_subject` AS select `r`.`report_id` AS `report_id`,`r`.`student_id` AS `student_id`,`r`.`grade` AS `grade`,`r`.`score` AS `score`,`r`.`chapter_id` AS `chapter_id`,`s`.`name` AS `name`,`c`.`chapter_name` AS `chapter_name`,`s2`.`subject_id` AS `subject_id`,`s2`.`subject_name` AS `subject_name`,avg(`r`.`score`) AS `average_score` from (((`report` `r` join `students` `s` on(`r`.`student_id` = `s`.`student_id`)) join `chapters` `c` on(`r`.`chapter_id` = `c`.`chapter_id`)) join `subjects` `s2` on(`s2`.`subject_id` = `c`.`subject_id`)) group by `r`.`chapter_id`,`s2`.`subject_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_max_average`
--

/*!50001 DROP TABLE IF EXISTS `v_max_average`*/;
/*!50001 DROP VIEW IF EXISTS `v_max_average`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`taufiq32`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_max_average` AS select `vabg`.`student_id` AS `student_id`,`vabg`.`name` AS `name`,max(`vabg`.`average_score`) AS `top`,`vabg`.`subject_name` AS `subject_name`,`vabg`.`chapter_name` AS `chapter_name` from `v_average_by_grade` `vabg` group by `vabg`.`student_id`,`vabg`.`subject_id` limit 3 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_min_average`
--

/*!50001 DROP TABLE IF EXISTS `v_min_average`*/;
/*!50001 DROP VIEW IF EXISTS `v_min_average`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`taufiq32`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_min_average` AS select `vabg`.`student_id` AS `student_id`,`vabg`.`name` AS `name`,min(`vabg`.`average_score`) AS `top`,`vabg`.`chapter_id` AS `chapter_id`,`vabg`.`subject_name` AS `subject_name`,`vabg`.`chapter_name` AS `chapter_name` from `v_average_by_grade` `vabg` group by `vabg`.`chapter_id` limit 3 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-17 14:27:15
